/* */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class S2 extends JFrame {
    JTextField textField;
    JButton bClick;
    String fileName;
    File file;

    S2() {
        setTitle("S2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    void init() {   // by default, a frame has BorderLayout
        this.add(textField = new JTextField(), BorderLayout.NORTH); //the upper part
        this.add(bClick = new JButton("Click"), BorderLayout.CENTER); // in the center
        bClick.addActionListener(new TratareButon());
    }

    class TratareButon implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            Scanner fileScanner = null;
            fileName = textField.getText(); //get the path of the file inserted by the user
            file = new File(fileName); //create a new object file for the given path
            try {
                StringBuilder sb = new StringBuilder();
                fileScanner = new Scanner(file);
                while (fileScanner.hasNextLine()) { // while the file has a next line, the line is read and appended to sb

                    sb.append(fileScanner.nextLine() + "\n");
                }
                System.out.println("The reverse is: " + sb.reverse()); //the reversed is displayed

            } catch (FileNotFoundException fileNotFoundException) { //in case the file is not found
                fileNotFoundException.printStackTrace();
            }


        }
    }

    public static void main(String[] args) {
        new S2();
    }

}
