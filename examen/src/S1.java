import java.util.List;

public class S1 {
    class A {

    }

    class B extends A //Inheritance
    {
        private long t;
        private D ds; // one-directional association

        public void b() {
        }

        public void displayC(C c) {
        } //dependency, the weakest relation
    }

    class D implements Y {
        private List<F> fs; //aggregation
        private E[] es; // composition, the link is much stronger than that of aggregation

        D(E[] es) {
            this.es = es;

        }

        public void met1(int i) {
        }
    }

    interface Y {
    }

    class E {
        public void met2() {
        }
    }

    class F {
    }

    class C {
    }

}
